import application = require("application");

application.setCssFileName("./styles/app.css");
application.start({ moduleName: "./pages/main/main" });
