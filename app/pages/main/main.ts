// tslint:disable:max-classes-per-file
// tslint:disable:object-literal-sort-keys
// tslint:disable:no-var-requires


import {EventData,
        Observable} from "data/observable";
import {Page}       from "ui/page";


class EventInfo {
    public eventName    : string;
    public viewID       : string;
    public eventHandler : (EventData) => void;
}


class MainPageController extends Observable {


    private page    : Page;
    private events  : EventInfo[];


    // Page loaded and unloaded events. This is where we bind / unbind our event
    // handers:


    public onPageLoaded(args: EventData): void {

        // store page property and set binding context:
        this.page = (args.object as Page);
        this.page.bindingContext = this;

        // Bind events, and store for later unbinding:
        this.events = this.bindEvents(

            [{eventName     : "textChange",
              viewID        : "textField1",
              eventHandler  : this.textChange1},

             {eventName     : "textChange",
              viewID        : "textField2",
              eventHandler  : this.textChange2},

        ]);

    }


    public onPageUnloaded(args: EventData): void {

        // Unbind events.
        //
        // This is *VERY* important. If you don't do this, subsequent page
        // unload-reloads will just add more of the same event handlers, so they
        // will get called multiple times, and they will take up more and more
        // memory.
        this.unbindEvents(this.events);

    }


    // Method for binding event handlers to multiple controls on the page:
    public bindEvents(events: EventInfo[]): EventInfo[] {

        events.forEach((event) => {
            const view = this.page.getViewById(event.viewID);
            if (view) {
                console.log("Binding: " + view);
                view.on(event.eventName, event.eventHandler, this);
            } else {
                // Error, no view with this id found:
                console.log("Error while attempting to bind view with id "
                            + event.viewID);
            }
        });

        // Return events array, for easy later unbinding:
        return events;

    }


    // Method for removing event handlers from multiple controls on the page:
    public unbindEvents(events: EventInfo[]): EventInfo[] {

        events.forEach((event) => {
            const view = this.page.getViewById(event.viewID);
            if (view) {
                console.log("Unbinding: " + view);
                view.off(event.eventName, event.eventHandler, this);
            } else {
                // Error, no view with this id found:
                console.log("Error while attempting to unbind view with id "
                            + event.viewID);
            }
        });

        // Just return events array, maybe useful for later binding again:
        return events;

    }


    // Some event handlers:
    public textChange1(args: EventData) {
        this.logObject("textChange1", args);
    }


    public textChange2(args: EventData) {
        this.logObject("textChange2", args);
    }


    // See what's in an object:
    private logObject(method: string, o: any): void {
        let result = method + "( args: {";
        const keys = Object.keys(o);
        keys.forEach((key) => {
            result += "\n    " + key + ": " + o[key];
        });
        result += "\n})";
        console.log(result);
    }


}


const mpc = new MainPageController();
export const onPageLoaded     = (args)  => mpc.onPageLoaded  (args);
export const onPageUnloaded   = (args)  => mpc.onPageUnloaded(args);
