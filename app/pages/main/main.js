// tslint:disable:max-classes-per-file
// tslint:disable:object-literal-sort-keys
// tslint:disable:no-var-requires
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var observable_1 = require("data/observable");
var EventInfo = (function () {
    function EventInfo() {
    }
    return EventInfo;
}());
var MainPageController = (function (_super) {
    __extends(MainPageController, _super);
    function MainPageController() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    // Page loaded and unloaded events. This is where we bind / unbind our event
    // handers:
    MainPageController.prototype.onPageLoaded = function (args) {
        // store page property and set binding context:
        this.page = args.object;
        this.page.bindingContext = this;
        // Bind events, and store for later unbinding:
        this.events = this.bindEvents([{ eventName: "textChange",
                viewID: "textField1",
                eventHandler: this.textChange1 },
            { eventName: "textChange",
                viewID: "textField2",
                eventHandler: this.textChange2 },
        ]);
    };
    MainPageController.prototype.onPageUnloaded = function (args) {
        // Unbind events.
        //
        // This is *VERY* important. If you don't do this, subsequent page
        // unload-reloads will just add more of the same event handlers, so they
        // will get called multiple times, and they will take up more and more
        // memory.
        this.unbindEvents(this.events);
    };
    // Method for binding event handlers to multiple controls on the page:
    MainPageController.prototype.bindEvents = function (events) {
        var _this = this;
        events.forEach(function (event) {
            var view = _this.page.getViewById(event.viewID);
            if (view) {
                console.log("Binding: " + view);
                view.on(event.eventName, event.eventHandler, _this);
            }
            else {
                // Error, no view with this id found:
                console.log("Error while attempting to bind view with id "
                    + event.viewID);
            }
        });
        // Return events array, for easy later unbinding:
        return events;
    };
    // Method for removing event handlers from multiple controls on the page:
    MainPageController.prototype.unbindEvents = function (events) {
        var _this = this;
        events.forEach(function (event) {
            var view = _this.page.getViewById(event.viewID);
            if (view) {
                console.log("Unbinding: " + view);
                view.off(event.eventName, event.eventHandler, _this);
            }
            else {
                // Error, no view with this id found:
                console.log("Error while attempting to unbind view with id "
                    + event.viewID);
            }
        });
        // Just return events array, maybe useful for later binding again:
        return events;
    };
    // Some event handlers:
    MainPageController.prototype.textChange1 = function (args) {
        this.logObject("textChange1", args);
    };
    MainPageController.prototype.textChange2 = function (args) {
        this.logObject("textChange2", args);
    };
    // See what's in an object:
    MainPageController.prototype.logObject = function (method, o) {
        var result = method + "( args: {";
        var keys = Object.keys(o);
        keys.forEach(function (key) {
            result += "\n    " + key + ": " + o[key];
        });
        result += "\n})";
        console.log(result);
    };
    return MainPageController;
}(observable_1.Observable));
var mpc = new MainPageController();
exports.onPageLoaded = function (args) { return mpc.onPageLoaded(args); };
exports.onPageUnloaded = function (args) { return mpc.onPageUnloaded(args); };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFpbi5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIm1haW4udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsc0NBQXNDO0FBQ3RDLDBDQUEwQztBQUMxQyxpQ0FBaUM7OztBQUdqQyw4Q0FDMkM7QUFJM0M7SUFBQTtJQUlBLENBQUM7SUFBRCxnQkFBQztBQUFELENBQUMsQUFKRCxJQUlDO0FBR0Q7SUFBaUMsc0NBQVU7SUFBM0M7O0lBK0dBLENBQUM7SUF4R0csNEVBQTRFO0lBQzVFLFdBQVc7SUFHSix5Q0FBWSxHQUFuQixVQUFvQixJQUFlO1FBRS9CLCtDQUErQztRQUMvQyxJQUFJLENBQUMsSUFBSSxHQUFJLElBQUksQ0FBQyxNQUFlLENBQUM7UUFDbEMsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDO1FBRWhDLDhDQUE4QztRQUM5QyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxVQUFVLENBRXpCLENBQUMsRUFBQyxTQUFTLEVBQU8sWUFBWTtnQkFDNUIsTUFBTSxFQUFVLFlBQVk7Z0JBQzVCLFlBQVksRUFBSSxJQUFJLENBQUMsV0FBVyxFQUFDO1lBRWxDLEVBQUMsU0FBUyxFQUFPLFlBQVk7Z0JBQzVCLE1BQU0sRUFBVSxZQUFZO2dCQUM1QixZQUFZLEVBQUksSUFBSSxDQUFDLFdBQVcsRUFBQztTQUV0QyxDQUFDLENBQUM7SUFFUCxDQUFDO0lBR00sMkNBQWMsR0FBckIsVUFBc0IsSUFBZTtRQUVqQyxpQkFBaUI7UUFDakIsRUFBRTtRQUNGLGtFQUFrRTtRQUNsRSx3RUFBd0U7UUFDeEUsc0VBQXNFO1FBQ3RFLFVBQVU7UUFDVixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUVuQyxDQUFDO0lBR0Qsc0VBQXNFO0lBQy9ELHVDQUFVLEdBQWpCLFVBQWtCLE1BQW1CO1FBQXJDLGlCQWlCQztRQWZHLE1BQU0sQ0FBQyxPQUFPLENBQUMsVUFBQyxLQUFLO1lBQ2pCLElBQU0sSUFBSSxHQUFHLEtBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNqRCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO2dCQUNQLE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxDQUFDO2dCQUNoQyxJQUFJLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxTQUFTLEVBQUUsS0FBSyxDQUFDLFlBQVksRUFBRSxLQUFJLENBQUMsQ0FBQztZQUN2RCxDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ0oscUNBQXFDO2dCQUNyQyxPQUFPLENBQUMsR0FBRyxDQUFDLDhDQUE4QztzQkFDNUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ2hDLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztRQUVILGlEQUFpRDtRQUNqRCxNQUFNLENBQUMsTUFBTSxDQUFDO0lBRWxCLENBQUM7SUFHRCx5RUFBeUU7SUFDbEUseUNBQVksR0FBbkIsVUFBb0IsTUFBbUI7UUFBdkMsaUJBaUJDO1FBZkcsTUFBTSxDQUFDLE9BQU8sQ0FBQyxVQUFDLEtBQUs7WUFDakIsSUFBTSxJQUFJLEdBQUcsS0FBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ2pELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7Z0JBQ1AsT0FBTyxDQUFDLEdBQUcsQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLENBQUM7Z0JBQ2xDLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLFNBQVMsRUFBRSxLQUFLLENBQUMsWUFBWSxFQUFFLEtBQUksQ0FBQyxDQUFDO1lBQ3hELENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDSixxQ0FBcUM7Z0JBQ3JDLE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0RBQWdEO3NCQUM5QyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDaEMsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO1FBRUgsa0VBQWtFO1FBQ2xFLE1BQU0sQ0FBQyxNQUFNLENBQUM7SUFFbEIsQ0FBQztJQUdELHVCQUF1QjtJQUNoQix3Q0FBVyxHQUFsQixVQUFtQixJQUFlO1FBQzlCLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ3hDLENBQUM7SUFHTSx3Q0FBVyxHQUFsQixVQUFtQixJQUFlO1FBQzlCLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ3hDLENBQUM7SUFHRCwyQkFBMkI7SUFDbkIsc0NBQVMsR0FBakIsVUFBa0IsTUFBYyxFQUFFLENBQU07UUFDcEMsSUFBSSxNQUFNLEdBQUcsTUFBTSxHQUFHLFdBQVcsQ0FBQztRQUNsQyxJQUFNLElBQUksR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzVCLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBQyxHQUFHO1lBQ2IsTUFBTSxJQUFJLFFBQVEsR0FBRyxHQUFHLEdBQUcsSUFBSSxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUM3QyxDQUFDLENBQUMsQ0FBQztRQUNILE1BQU0sSUFBSSxNQUFNLENBQUM7UUFDakIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUN4QixDQUFDO0lBR0wseUJBQUM7QUFBRCxDQUFDLEFBL0dELENBQWlDLHVCQUFVLEdBK0cxQztBQUdELElBQU0sR0FBRyxHQUFHLElBQUksa0JBQWtCLEVBQUUsQ0FBQztBQUN4QixRQUFBLFlBQVksR0FBTyxVQUFDLElBQUksSUFBTSxPQUFBLEdBQUcsQ0FBQyxZQUFZLENBQUcsSUFBSSxDQUFDLEVBQXhCLENBQXdCLENBQUM7QUFDdkQsUUFBQSxjQUFjLEdBQUssVUFBQyxJQUFJLElBQU0sT0FBQSxHQUFHLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxFQUF4QixDQUF3QixDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiLy8gdHNsaW50OmRpc2FibGU6bWF4LWNsYXNzZXMtcGVyLWZpbGVcbi8vIHRzbGludDpkaXNhYmxlOm9iamVjdC1saXRlcmFsLXNvcnQta2V5c1xuLy8gdHNsaW50OmRpc2FibGU6bm8tdmFyLXJlcXVpcmVzXG5cblxuaW1wb3J0IHtFdmVudERhdGEsXG4gICAgICAgIE9ic2VydmFibGV9IGZyb20gXCJkYXRhL29ic2VydmFibGVcIjtcbmltcG9ydCB7UGFnZX0gICAgICAgZnJvbSBcInVpL3BhZ2VcIjtcblxuXG5jbGFzcyBFdmVudEluZm8ge1xuICAgIHB1YmxpYyBldmVudE5hbWUgICAgOiBzdHJpbmc7XG4gICAgcHVibGljIHZpZXdJRCAgICAgICA6IHN0cmluZztcbiAgICBwdWJsaWMgZXZlbnRIYW5kbGVyIDogKEV2ZW50RGF0YSkgPT4gdm9pZDtcbn1cblxuXG5jbGFzcyBNYWluUGFnZUNvbnRyb2xsZXIgZXh0ZW5kcyBPYnNlcnZhYmxlIHtcblxuXG4gICAgcHJpdmF0ZSBwYWdlICAgIDogUGFnZTtcbiAgICBwcml2YXRlIGV2ZW50cyAgOiBFdmVudEluZm9bXTtcblxuXG4gICAgLy8gUGFnZSBsb2FkZWQgYW5kIHVubG9hZGVkIGV2ZW50cy4gVGhpcyBpcyB3aGVyZSB3ZSBiaW5kIC8gdW5iaW5kIG91ciBldmVudFxuICAgIC8vIGhhbmRlcnM6XG5cblxuICAgIHB1YmxpYyBvblBhZ2VMb2FkZWQoYXJnczogRXZlbnREYXRhKTogdm9pZCB7XG5cbiAgICAgICAgLy8gc3RvcmUgcGFnZSBwcm9wZXJ0eSBhbmQgc2V0IGJpbmRpbmcgY29udGV4dDpcbiAgICAgICAgdGhpcy5wYWdlID0gKGFyZ3Mub2JqZWN0IGFzIFBhZ2UpO1xuICAgICAgICB0aGlzLnBhZ2UuYmluZGluZ0NvbnRleHQgPSB0aGlzO1xuXG4gICAgICAgIC8vIEJpbmQgZXZlbnRzLCBhbmQgc3RvcmUgZm9yIGxhdGVyIHVuYmluZGluZzpcbiAgICAgICAgdGhpcy5ldmVudHMgPSB0aGlzLmJpbmRFdmVudHMoXG5cbiAgICAgICAgICAgIFt7ZXZlbnROYW1lICAgICA6IFwidGV4dENoYW5nZVwiLFxuICAgICAgICAgICAgICB2aWV3SUQgICAgICAgIDogXCJ0ZXh0RmllbGQxXCIsXG4gICAgICAgICAgICAgIGV2ZW50SGFuZGxlciAgOiB0aGlzLnRleHRDaGFuZ2UxfSxcblxuICAgICAgICAgICAgIHtldmVudE5hbWUgICAgIDogXCJ0ZXh0Q2hhbmdlXCIsXG4gICAgICAgICAgICAgIHZpZXdJRCAgICAgICAgOiBcInRleHRGaWVsZDJcIixcbiAgICAgICAgICAgICAgZXZlbnRIYW5kbGVyICA6IHRoaXMudGV4dENoYW5nZTJ9LFxuXG4gICAgICAgIF0pO1xuXG4gICAgfVxuXG5cbiAgICBwdWJsaWMgb25QYWdlVW5sb2FkZWQoYXJnczogRXZlbnREYXRhKTogdm9pZCB7XG5cbiAgICAgICAgLy8gVW5iaW5kIGV2ZW50cy5cbiAgICAgICAgLy9cbiAgICAgICAgLy8gVGhpcyBpcyAqVkVSWSogaW1wb3J0YW50LiBJZiB5b3UgZG9uJ3QgZG8gdGhpcywgc3Vic2VxdWVudCBwYWdlXG4gICAgICAgIC8vIHVubG9hZC1yZWxvYWRzIHdpbGwganVzdCBhZGQgbW9yZSBvZiB0aGUgc2FtZSBldmVudCBoYW5kbGVycywgc28gdGhleVxuICAgICAgICAvLyB3aWxsIGdldCBjYWxsZWQgbXVsdGlwbGUgdGltZXMsIGFuZCB0aGV5IHdpbGwgdGFrZSB1cCBtb3JlIGFuZCBtb3JlXG4gICAgICAgIC8vIG1lbW9yeS5cbiAgICAgICAgdGhpcy51bmJpbmRFdmVudHModGhpcy5ldmVudHMpO1xuXG4gICAgfVxuXG5cbiAgICAvLyBNZXRob2QgZm9yIGJpbmRpbmcgZXZlbnQgaGFuZGxlcnMgdG8gbXVsdGlwbGUgY29udHJvbHMgb24gdGhlIHBhZ2U6XG4gICAgcHVibGljIGJpbmRFdmVudHMoZXZlbnRzOiBFdmVudEluZm9bXSk6IEV2ZW50SW5mb1tdIHtcblxuICAgICAgICBldmVudHMuZm9yRWFjaCgoZXZlbnQpID0+IHtcbiAgICAgICAgICAgIGNvbnN0IHZpZXcgPSB0aGlzLnBhZ2UuZ2V0Vmlld0J5SWQoZXZlbnQudmlld0lEKTtcbiAgICAgICAgICAgIGlmICh2aWV3KSB7XG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJCaW5kaW5nOiBcIiArIHZpZXcpO1xuICAgICAgICAgICAgICAgIHZpZXcub24oZXZlbnQuZXZlbnROYW1lLCBldmVudC5ldmVudEhhbmRsZXIsIHRoaXMpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAvLyBFcnJvciwgbm8gdmlldyB3aXRoIHRoaXMgaWQgZm91bmQ6XG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJFcnJvciB3aGlsZSBhdHRlbXB0aW5nIHRvIGJpbmQgdmlldyB3aXRoIGlkIFwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKyBldmVudC52aWV3SUQpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcblxuICAgICAgICAvLyBSZXR1cm4gZXZlbnRzIGFycmF5LCBmb3IgZWFzeSBsYXRlciB1bmJpbmRpbmc6XG4gICAgICAgIHJldHVybiBldmVudHM7XG5cbiAgICB9XG5cblxuICAgIC8vIE1ldGhvZCBmb3IgcmVtb3ZpbmcgZXZlbnQgaGFuZGxlcnMgZnJvbSBtdWx0aXBsZSBjb250cm9scyBvbiB0aGUgcGFnZTpcbiAgICBwdWJsaWMgdW5iaW5kRXZlbnRzKGV2ZW50czogRXZlbnRJbmZvW10pOiBFdmVudEluZm9bXSB7XG5cbiAgICAgICAgZXZlbnRzLmZvckVhY2goKGV2ZW50KSA9PiB7XG4gICAgICAgICAgICBjb25zdCB2aWV3ID0gdGhpcy5wYWdlLmdldFZpZXdCeUlkKGV2ZW50LnZpZXdJRCk7XG4gICAgICAgICAgICBpZiAodmlldykge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiVW5iaW5kaW5nOiBcIiArIHZpZXcpO1xuICAgICAgICAgICAgICAgIHZpZXcub2ZmKGV2ZW50LmV2ZW50TmFtZSwgZXZlbnQuZXZlbnRIYW5kbGVyLCB0aGlzKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgLy8gRXJyb3IsIG5vIHZpZXcgd2l0aCB0aGlzIGlkIGZvdW5kOlxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiRXJyb3Igd2hpbGUgYXR0ZW1wdGluZyB0byB1bmJpbmQgdmlldyB3aXRoIGlkIFwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKyBldmVudC52aWV3SUQpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcblxuICAgICAgICAvLyBKdXN0IHJldHVybiBldmVudHMgYXJyYXksIG1heWJlIHVzZWZ1bCBmb3IgbGF0ZXIgYmluZGluZyBhZ2FpbjpcbiAgICAgICAgcmV0dXJuIGV2ZW50cztcblxuICAgIH1cblxuXG4gICAgLy8gU29tZSBldmVudCBoYW5kbGVyczpcbiAgICBwdWJsaWMgdGV4dENoYW5nZTEoYXJnczogRXZlbnREYXRhKSB7XG4gICAgICAgIHRoaXMubG9nT2JqZWN0KFwidGV4dENoYW5nZTFcIiwgYXJncyk7XG4gICAgfVxuXG5cbiAgICBwdWJsaWMgdGV4dENoYW5nZTIoYXJnczogRXZlbnREYXRhKSB7XG4gICAgICAgIHRoaXMubG9nT2JqZWN0KFwidGV4dENoYW5nZTJcIiwgYXJncyk7XG4gICAgfVxuXG5cbiAgICAvLyBTZWUgd2hhdCdzIGluIGFuIG9iamVjdDpcbiAgICBwcml2YXRlIGxvZ09iamVjdChtZXRob2Q6IHN0cmluZywgbzogYW55KTogdm9pZCB7XG4gICAgICAgIGxldCByZXN1bHQgPSBtZXRob2QgKyBcIiggYXJnczoge1wiO1xuICAgICAgICBjb25zdCBrZXlzID0gT2JqZWN0LmtleXMobyk7XG4gICAgICAgIGtleXMuZm9yRWFjaCgoa2V5KSA9PiB7XG4gICAgICAgICAgICByZXN1bHQgKz0gXCJcXG4gICAgXCIgKyBrZXkgKyBcIjogXCIgKyBvW2tleV07XG4gICAgICAgIH0pO1xuICAgICAgICByZXN1bHQgKz0gXCJcXG59KVwiO1xuICAgICAgICBjb25zb2xlLmxvZyhyZXN1bHQpO1xuICAgIH1cblxuXG59XG5cblxuY29uc3QgbXBjID0gbmV3IE1haW5QYWdlQ29udHJvbGxlcigpO1xuZXhwb3J0IGNvbnN0IG9uUGFnZUxvYWRlZCAgICAgPSAoYXJncykgID0+IG1wYy5vblBhZ2VMb2FkZWQgIChhcmdzKTtcbmV4cG9ydCBjb25zdCBvblBhZ2VVbmxvYWRlZCAgID0gKGFyZ3MpICA9PiBtcGMub25QYWdlVW5sb2FkZWQoYXJncyk7XG4iXX0=